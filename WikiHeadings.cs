﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebScrapper
{

    public class WikiHeadings
    {
        public string Link { get; set; }
        public string Metadata { get; set; }
        public string PepName { get; set; }
        public string Country_Name { get; set; }
        public string Designation { get; set; }
        public string WikiOrNot { get; set; }
        public string SearchString { get; set; }
        public string PepnamecheckInlink { get; set; }
        public string DataField { get; set; }
        public string Words_to_check_in_metadata { get; set; }
        public Guid PepUid { get; set; }
        public string SCU { get; set; }
        public long PepAllocationId { get; set; }
      

    }
}
