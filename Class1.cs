﻿// 1.44pm
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
namespace WebScrapper
{
    public class Class1:WikiHeadings
    {
        public static List<WikiHeadings> WikiVcard(WikiHeadings pep_With_Wiki) 
        {
           
            List<WikiHeadings> pep_With_Wikis = new List<WikiHeadings>();
            using (var client = new WebClient())
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(client.DownloadString(pep_With_Wiki.Link));

                //HtmlNodeCollection infobox_vcard = doc.DocumentNode.SelectNodes("//table[@class=\"infobox vcard\"]");
                HtmlNodeCollection infobox_vcard = doc.DocumentNode.SelectNodes("//table[contains(@class,\"infobox\")]");////tr[th[@scope=\"row\"]]
                string UrlString = string.Empty,UrlString2="";
                if (infobox_vcard != null)
                {
                    foreach (HtmlNode node in infobox_vcard)
                    {
                        UrlString = UrlString + "  " + node.InnerText;
                        UrlString = Regex.Replace(UrlString, @"&#[0-9][0-9]", " ");
                        pep_With_Wiki.SCU = UrlString;
                        pep_With_Wiki.DataField = "DOB, Alias, Position, Address, Relationship";
                        pep_With_Wikis.Add(pep_With_Wiki);

                    }
                }
                List<WikiHeadings> ExtractWikiHeadings = new List<WikiHeadings>();

                //List<string> Headings = new List<string>
                //{
                //    "Career",
                //    "Early life",
                //    "Position",
                //    "Politics",
                //    "Background"
                //};
                UrlString = string.Empty;
                HtmlNodeCollection body_of_wiki = doc.DocumentNode.SelectNodes("//body");
                if (body_of_wiki != null)
                {
                    foreach (HtmlNode node in body_of_wiki)
                    {
                        UrlString = UrlString + " " + node.InnerText;
                    }
                    UrlString = Regex.Replace(UrlString, "<span .*?</span>", string.Empty);
                    UrlString = Regex.Replace(UrlString, "<style .*?</style>", string.Empty);
                    UrlString = Regex.Replace(UrlString, @"\s+", " ");
                    MatchCollection PoliticalCareer = Regex.Matches(UrlString, @"(?i)(From Wikipedia, the free encyclopedia|Career|Personal Life|Early Life|Education|Honours|Political career|Background|Member of parliament|Professional Career|Family|Biography|Local Politics)"); //Political Career
                                                                                      //From Wikipedia, the free encyclopedia
                    foreach (Match match in PoliticalCareer)
                    {
                        
                        if (UrlString.Length >= 250)
                            UrlString2 = UrlString.Substring(match.Index,250);
                        else
                        {
                            UrlString2 = UrlString.Substring(match.Index, UrlString.Length-match.Index);
                            
                        }
                        Console.WriteLine(UrlString);

                        pep_With_Wiki.SCU = UrlString2;

                        pep_With_Wiki.DataField = "DOB, POB, Relationship, Position, Address, Alias";

                        pep_With_Wikis.Add(pep_With_Wiki);
                    }

                }
                return pep_With_Wikis;
            }
        }
    }
}
